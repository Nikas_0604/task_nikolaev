#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <signal.h>
#include <netinet/ip.h>

#define ADDRESS "mysocket" /* адрес для связи */
#define STRMAX 40          /* максимальная длина сообщения */
int fd[2],fdd[2],pd[2];    
int fnum[2];
int number = 1;    /* число на которое сервер будет увеличивать другие числа */
int clients = 0;   /* кол-во клиентов */
int temp_sock;     /* сокет-дескриптор */

/*==================================================
 * обработчик сигнала для сына, 
 * изменение глобального параметра number
 *==================================================
*/

int readbase(char *str){
	char str1[STRMAX]="";
	FILE *fp;
	fp=fopen("base.txt","r");
	while (!feof(fp)){
		fgets(str1,STRMAX,fp);
		if (strcmp(str1,str)==0){
			fclose(fp);
			return 1;
		}
	}
	fclose(fp);
	return 0;
}		

void del(char *str){
	FILE *fp,*fp1;
	char str1[STRMAX];
	fp=fopen("base.txt","r");
	fp1=fopen("temp.txt","w");
	while (!feof(fp)){
		fgets(str1,STRMAX,fp);
		if (!feof(fp))
			fputs(str1,fp1);
	}
	fclose(fp);
	fclose(fp1);	
	fp1=fopen("temp.txt","r");
	fp=fopen("base.txt","w");
	int flag=0;
	while (!feof(fp1)){
		fgets(str1,STRMAX,fp1);
		if (!feof(fp1)){
			if (strcmp(str1,str)!=0 || flag==1)
				fputs(str1,fp);
			else
				flag=1;
		}
	}
	fclose(fp);
	fclose(fp1);
}

void add(char *str){
	FILE *fp;
	fp=fopen("base.txt","a");
	fputs(str,fp);
	fclose(fp);
}

void SigSon(int n) {
	int i;
	char buf[STRMAX];
	read(fd[0], buf, STRMAX);
	for (i=0;i<clients;i++)
		write(fd[1],buf,STRMAX);
	kill(0,SIGUSR2);	
}

void SigSon2(int n) {
	int i;
	char buf[STRMAX];
	read(fdd[0], buf, STRMAX);
	for (i=0;i<clients;i++)
		write(fdd[1],buf,STRMAX);
	kill(0,SIGABRT);	
}

void SigDad(int n) {
	char buf[STRMAX];
	read(fd[0], buf, STRMAX);
	send(temp_sock, buf, STRMAX, 0);
}

void SigGrpa(int n){
	char buf[STRMAX];
	read(fdd[0], buf, STRMAX);
	send(temp_sock, buf, STRMAX, 0);
}

void SigClient(int n) {
	clients--;
	printf("now there are %d users\n",clients);
	printf("-----\n");
}

int main (int argc, char ** argv) {
	char str[STRMAX],car[STRMAX];
	int  main_sock, len, temp_len, port;       
	struct sockaddr_in main_adr, temp_adr;
	pipe(fd);     /* канал для передачи глобального параметра number */
	pipe(pd);
	pipe(fdd);
	pipe(fnum);
	printf("server begin\n");
	printf("-----\n");
	
	
	/* получаем свой сокет-дескриптор: */
	if ( (main_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) { 
		perror("client: socket");
		exit(1);
	}
	/* создаем адрес, c которым будут связываться клиенты */
	//main_adr.sun_family = AF_UNIX;
	//strcpy(main_adr.sun_path, ADDRESS);
	
	sscanf(argv[1], "%d", &port);
    	main_adr.sin_family = AF_INET;
    	main_adr.sin_port = htons(port);
    	main_adr.sin_addr.s_addr = INADDR_ANY;
	
	/* связываем адрес с сокетом; уничтожаем файл с именем ADDRESS, 
	 * если он существует, для того, чтобы вызов bind завершился успешно
	*/
	unlink(ADDRESS);
	len = sizeof(main_adr);
	if ( bind(main_sock, (struct sockaddr *) &main_adr, len) < 0 ) {
		perror("server: bind"); 
		exit(1);
	}
	/* слушаем запросы на сокет */
	if ( listen(main_sock, 5) < 0 ) {
		perror("server: listen"); 
		exit(1); 
	}

	write(fnum[1], " ", STRMAX);
	for(;;){
		/* связываемся с клиентом через неименованный сокет с дескриптором d1: */
		temp_len = sizeof temp_adr; 
		if ( (temp_sock = accept(main_sock, (struct sockaddr *) &temp_adr, &temp_len)) < 0 ) {
			perror("server: accept");
			exit(1);
		}
		if (!fork()){
				if (fork()){
					int grandpa;
					grandpa=getppid();
					write(pd[1],&grandpa,sizeof(int));
					signal(SIGUSR2, SigDad);
					signal(SIGUSR1, SIG_IGN);
					signal(SIGTERM, SIG_IGN);
					signal(SIGABRT, SIG_IGN);
					for(;;){
						fgets(str,STRMAX,stdin); 
						printf("-----\n");
						if (strcmp(str,"EXIT\n")==0){
							kill(0, SIGINT);
							break;
						}
						else{
							write(fd[1],str,STRMAX);
							kill(getppid(),SIGUSR1);
						}
					}
				}
				else{
					int c;
					signal(SIGUSR2, SIG_IGN);
					signal(SIGUSR1, SIG_IGN);
					signal(SIGTERM, SIG_IGN);
					signal(SIGABRT, SigGrpa);
					read(pd[0],&c,sizeof(c));
					for(;;){
						recv(temp_sock, str, STRMAX, 0);
						if (strcmp(str,"EXIT\n")==0){
							kill(c,SIGILL);
							break;
						}
						else if (str[0]=='B' && str[1] =='U' && str[2]=='Y' && str[3]==' '){
							strcpy(car,str+4);	
							if(readbase(car)){
								del(car);
								send(temp_sock, "YES", STRMAX, 0);
								strncat(car,"has been sold\n",STRMAX/2);
								write(fdd[1],car,STRMAX);
								kill(c,SIGTERM);
							}
							else
								send(temp_sock, "NO", STRMAX, 0);
						}
						else if (str[0]=='S' && str[1] =='E' && str[2]=='L' && str[3]=='L' && str[4]==' '){ 
							strcpy(car,str+5);
							add(car);
							strncat(car,"can be bought\n",STRMAX/2);
							write(fdd[1],car,STRMAX);
							kill(c,SIGTERM);
						}	
					}
				}		
		}
		else {
			clients++;
			printf("now there are %d users\n",clients);
			printf("-----\n");
			signal(SIGUSR2, SIG_IGN);
			signal(SIGUSR1, SigSon);
			signal(SIGTERM, SigSon2);
			signal(SIGABRT, SIG_IGN);
			signal(SIGILL, SigClient);
			/* закрывает сокет и разрывает все соединения с этим сокетом */
			close(temp_sock);
			continue;
		}	
	}			
}		
		
		
		
		
		
