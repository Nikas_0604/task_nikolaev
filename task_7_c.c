#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/ip.h>
#include <signal.h>

#define ADDRESS "mysocket"  /* адрес для связи */
#define STRMAX 40           /* максимальная длина сообщения */

int main (int argc, char ** argv) {
	char str[STRMAX]="",str1[STRMAX]="",*temp;  
	int i, main_sock, len, port;
	struct sockaddr_in main_adr;
	printf("client begin\n");
	printf("-----\n");
	
	/* получаем сокет-дескриптор: */
	if ( (main_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("client: socket");
		exit(1);
	}
	
	/* создаем адрес, по которому будем связываться с сервером: */
	//main_adr.sun_family = AF_UNIX;
	//strcpy(main_adr.sun_path, ADDRESS);
	
	/* port = atoi(argv[1]); */
	sscanf(argv[1], "%d", &port);
     
    	main_adr.sin_family = AF_INET;
    	main_adr.sin_port = htons(port);
    	main_adr.sin_addr.s_addr = INADDR_ANY;
    	
	
	/* пытаемся связаться с сервером (с адресом ADDRESS): */
	len = sizeof(main_adr);
	if ( connect(main_sock, (struct sockaddr *) &main_adr, len) < 0 ) { 
		perror("client: connect");
		exit(1);
	}
	if (fork()){
		for(;;){
			fgets(str,STRMAX,stdin);
			printf("-----\n");
			if (strcmp(str, "EXIT\n") == 0) {
				send(main_sock, str, STRMAX, 0);
				sleep(0.1);
				kill(0, SIGKILL);
				break;
			}
			else if ((str[0]!='B' || str[1] !='U' || str[2]!='Y' || str[3]!=' ') && (str[0]!='S' || str[1] !='E' || str[2]!='L' || str[3]!='L' || str[4]!=' ')){
				printf("incorrect format\n");
				printf("-----\n");
			}
			else{
				send(main_sock, str, STRMAX, 0);
			
			}
			fflush(stdin);
			fseek(stdin, 0, SEEK_END);
			fflush(stdin);
		}
	}
	else{ 
		for(;;){
			recv(main_sock, str1, STRMAX, 0);
			if (strcmp(str1,"YES")==0)
				printf("thanks for your purchase!\n");
			else if (strcmp(str1,"NO")==0)
				printf("this car is missing!\n");
			else 
				printf("%s",str1);
			printf("-----\n");
		}
	}
	/* закрывает сокет и разрывает все соединения с этим сокетом */
	close(main_sock);
	printf("client stoped\n");
	exit(0);
}
